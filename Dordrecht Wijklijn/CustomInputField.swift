//
//  CustomInputField.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 26/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//


import Foundation
import UIKit

class CustomInputField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let shadowPath = UIBezierPath(rect: bounds)
        let paddingView = UIView(frame: CGRectMake(0, 0, 20, 0))
        
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.Always
        
        self.layer.cornerRadius = 2.0;
        self.backgroundColor = UIColor.whiteColor()
        
        self.font = UIFont(name: "Aller", size: 14)
        self.textColor = UIColor.init(colorLiteralRed: 0.5, green: 0.52, blue: 0.54, alpha: 1.0)
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.08).CGColor
        self.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        self.layer.shadowOpacity = 1.0
    }
}