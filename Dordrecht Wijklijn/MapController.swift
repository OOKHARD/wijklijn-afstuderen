//
//  MapController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 28/04/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    /*
        Map Variables
    */
    var userLocation                        : CLLocation?
    var googleMaps                          : GMSMapView?
    var openMarker                          : GMSMarker?
    
    /*
        Map let Variables
    */
    let regionRadius                        : CLLocationDistance = 1000
    let locationManager                     = CLLocationManager()
    let shareData                           = ShareData.sharedInstance
    
    /*
        UI Element connection with view.
    */
    @IBOutlet weak var contactButton: UIBarButtonItem!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var comLayer: UIView!
    @IBOutlet weak var infoImage: UIImageView!
    @IBOutlet weak var infoTitle: UILabel!
    @IBOutlet weak var infoDesc: UITextView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var pollInfoView: UIView!
    @IBOutlet weak var pollQuestion: UITextView!
    @IBOutlet weak var pollAnswer1: CustomInputField!
    @IBOutlet weak var pollAnswer2: CustomInputField!
    @IBOutlet weak var pollAnswer3: CustomInputField!
    @IBOutlet weak var pollAnswer4: CustomInputField!
    @IBOutlet weak var correct1: UIImageView!
    @IBOutlet weak var correct2: UIImageView!
    @IBOutlet weak var correct3: UIImageView!
    @IBOutlet weak var correct4: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Hide communication menu and infoview of markers.
        comLayer.hidden = true
        infoView.hidden = true
        pollInfoView.hidden = true
        
        //Start location authorization.
        self.locationManager.requestWhenInUseAuthorization()
        
        //Check if location use is authorizted
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //Init google maps
        let camera = GMSCameraPosition.cameraWithLatitude(51.811888, longitude: 4.663728, zoom: 16)
        googleMaps = GMSMapView.mapWithFrame(mapView.bounds, camera: camera)
        googleMaps!.myLocationEnabled = true
        googleMaps!.delegate = self
        googleMaps!.trafficEnabled = true
        mapView.addSubview(self.googleMaps!)
    }
    
    //viewDidAppear fires when view is active in application
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       
        //Remove markers from maps and get al the markers.
        googleMaps?.clear()
        getPoi("http://ookhard.nl/school/getPOI.php")
        getPoll("http://ookhard.nl/school/getPoll.php")
    }
    
    //prepareForSegue fires when segue is performed.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        //Hide navigationbar when segueID is chatSegue
        if segue.identifier == "chatSegue" {
            
            //Hide navigationbar in chatView.
            let destinationController = segue.destinationViewController as! ChatController
            destinationController.hidesBottomBarWhenPushed = true
            
            //Hide communication menu.
            hideComLayer()
        }
        
        //Hide bottomtabbar when detailSegue is performed
        if segue.identifier == "detailSegue" {
            let destinationController = segue.destinationViewController as! MarkerDetailController
            destinationController.hidesBottomBarWhenPushed = true
        }
    }
    
    /*
        Button registration for contactbutton in top navigation bar.
    */
    @IBAction func contactButton(sender: AnyObject) {
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Button registration for phoneButton let application call number.
        TODO: Add phonennumber Dordrecht
    */
    @IBAction func phoneButton(sender: AnyObject) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://0100000000") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
                hideComLayer()
            }
        }
    }
    
    /*
        Button registration for phoneButton open mailapplication and mail to info@dordrecht.nl
        TODO: Check emailadres Dordrecht
    */
    @IBAction func mailButton(sender: AnyObject) {
        if let emailURL:NSURL = NSURL(string: "mailto:info@dordrecht.nl") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(emailURL)) {
                application.openURL(emailURL);
            }
        }
        
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Markerinfo closeButton registration
    */
    @IBAction func closeButton(sender: AnyObject) {
        infoView.hidden = true
        openMarker?.opacity = 1
    }
    
    /*
        Pollinformation pollCloseButton registration
    */
    @IBAction func pollCloseButton(sender: AnyObject) {
        pollInfoView.hidden = true
        openMarker?.opacity = 1
        correct1.hidden = true
        correct2.hidden = true
        correct3.hidden = true
        correct4.hidden = true
    }
    
    /*
        markerInfo moreInfoButton registration
    */
    @IBAction func moreInfoButton(sender: AnyObject) {
        infoView.hidden = true
    }
    
    /*
        Pollinformation sendPollButton registration
    */
    @IBAction func sendPollButton(sender: AnyObject) {
        //Hides pollInfoView
        pollInfoView.hidden = true
        
        //Add popup to view when poll is send.
        let alert = UIAlertController(title: "Poll ingezonden", message: "Uw poll is succesvol verzonden naar de gemeente!", preferredStyle: UIAlertControllerStyle.Alert)
        self.presentViewController(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                self.navigationController?.popViewControllerAnimated(true)
                
            case .Cancel: break
                
            case .Destructive: break
            }
        }))
    }
    
    /*
        Pollinformation answer1Button registration, show what answer is checked.
    */
    @IBAction func answer1Button(sender: AnyObject) {
        correct1.hidden = false
        correct2.hidden = true
        correct3.hidden = true
        correct4.hidden = true
    }
    
    /*
        Pollinformation answer2Button registration, show what answer is checked.
    */
    @IBAction func answer2Button(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = false
        correct3.hidden = true
        correct4.hidden = true
    }
    
    /*
        Pollinformation answer3Button registration, show what answer is checked.
    */
    @IBAction func answer3Button(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = true
        correct3.hidden = false
        correct4.hidden = true
    }
    
    /*
        Pollinformation answer4Button registration, show what answer is checked.
    */
    @IBAction func answer4Button(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = true
        correct3.hidden = true
        correct4.hidden = false
    }

    /*
        Function for google maps, fire event when user tap on marker.
    */
    func mapView(googleMaps: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        
        //Check if marker has werkzaamheden as category, if it is werkzaamheden fill moreinfo with correct information
        if marker.snippet == "Werkzaamheden" {
            infoImage.image = UIImage(named: "S-Werk image")
            infoImage.frame = CGRectMake(12, 16, 24, 14)
            let markerurl = String("http://ookhard.nl/school/getMarker.php?id=\(marker.title!)")
            self.shareData.markerID = Int(marker.title!)
            getMarker(markerurl)
            infoView.hidden = true

        }
        
        //Check if marker has evenement as category, if it is evenement fill moreinfo with correct information
        if marker.snippet == "Evenement"{
            infoImage.image = UIImage(named: "S-Event image")
            infoImage.frame = CGRectMake(12, 14, 22, 22)
            let markerurl = String("http://ookhard.nl/school/getMarker.php?id=\(marker.title!)")
            self.shareData.markerID = Int(marker.title!)
            getMarker(markerurl)
            infoView.hidden = true
        }
        
        //Check if marker has poll as category, if it is poll fill pollinfo with correct information
        if marker.snippet == "poll" {
            
            if marker.opacity != 0.3 {
                let pollurl = String("http://ookhard.nl/school/getSinglePoll.php?id=\(marker.title!)")
                getSinglePoll(pollurl)
            }
            
            openMarker?.opacity =  0.3
        }
        
        openMarker = marker

        return true
    }
    
    /*
        Function for updating location. This function is only called ones because we don't need continous location updates.
    */
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Set userlocation in variable
        userLocation = CLLocation(latitude: manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude)
        
        //Animate googlemaps camera to userlocation.
        googleMaps?.animateToLocation((userLocation?.coordinate)!)
        
        //Stop updating location function.
        locationManager.stopUpdatingLocation()
    }
    
    /*
        Error function for location.
    */
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    
    /*
        Function for getting json marker data from url
    */
    func getMarker(markerUrl: String) {
        //Set url
        let url = NSURL(string: markerUrl)
        
        //Set task in let.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                //Fire function updatemarker with parameter data in json.
                self.updateMarker(data!)
            })
        }
        
        task.resume()
    }
    
    /*
        Function for looping through json marker data
    */
    func updateMarker(markerData: NSData) {
        do {
            //Setting json data in let variable
            let json = try NSJSONSerialization.JSONObjectWithData(markerData, options: .AllowFragments)
            
            //Loop through items and set UI elements
            if let pois = json["items"] as? [[String: AnyObject]] {
                for poi in pois {
                    self.infoTitle.text = poi["title"] as? String
                    self.infoDesc.text = poi["desc_kort"] as! String
                }
            }
            
            //Hide pollinfoview and show infoView
            pollInfoView.hidden = true
            infoView.hidden = false
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    /*
        Function for getting json singlePoll data from url
    */
    func getSinglePoll(pollUrl: String) {
        //Set url
        let url = NSURL(string: pollUrl)
        
        //Set task in let
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                //Fire function updateSinglePoll with parameter data
                self.updateSinglePoll(data!)
            })
        }
        
        task.resume()
    }
    
    /*
        Function for looping through json singlePoll data
    */
    func updateSinglePoll(pollData: NSData) {
        do {
            //Setting json data in let variable
            let json = try NSJSONSerialization.JSONObjectWithData(pollData, options: .AllowFragments)
            
            //Loop through items and set UI elements
            if let pois = json["items"] as? [[String: AnyObject]] {
                for poi in pois {
                    self.pollQuestion.text = poi["question"] as! String
                    self.pollQuestion.font = UIFont(name: "Aller", size: 15)
                    self.pollAnswer1.text = poi["answer1"] as? String
                    self.pollAnswer2.text = poi["answer2"] as? String
                    self.pollAnswer3.text = poi["answer3"] as? String
                    self.pollAnswer4.text = poi["answer4"] as? String
                }
            }
            
            //Show pollinfoview and hide infoView
            infoView.hidden = true
            pollInfoView.hidden = false
        } catch {
            print("error serializing JSON: \(error)")
        }
    }

    /*
        Function for getting json Points of Interest data from url
    */
    func getPoi(poiUrl: String) {
        //Set url
        let url = NSURL(string: poiUrl)
        
        //Set task in let
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                //Fire function pushMarker with parameter data
                self.pushMarker(data!)
            })
        }
        
        task.resume()
    }
    
    /*
        Function for looping through json Marker data
    */
    func pushMarker(poiData: NSData) {
        
        do {
            //Setting json data to variable
            let json = try NSJSONSerialization.JSONObjectWithData(poiData, options: .AllowFragments)
            
            //Looping through all markeritems
            if let pois = json["items"] as? [[String: AnyObject]] {
                for poi in pois {
                    //setting marker data with json data
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2DMake(Double(poi["latitude"] as! String)!, Double(poi["longitude"] as! String)!)
                    marker.title = poi["id"] as? String
                    marker.snippet = poi["categorie"] as? String
                    
                    //Check wich categorie belongs to marker and change marker image to correct categorie image.
                    if poi["categorie"] as! String == "Werkzaamheden" {
                        marker.icon = UIImage(named: "Werk image")
                    } else {
                        marker.icon = UIImage(named: "Event image")
                    }
                    
                    //Add marker to googleMaps
                    marker.map = googleMaps
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    /*
        Function for getting json poll data from url
    */
    func getPoll(pollUrl: String) {
        //Set url
        let url = NSURL(string: pollUrl)
        
        //Set task in let
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                //Fire function pushPoll with parameter data.
                self.pushPoll(data!)
            })
        }
        
        task.resume()
    }
    
    /*
        Function for looping through json poll data
    */
    func pushPoll(pollData: NSData) {
        do {
            //Setting json data to variable
            let json = try NSJSONSerialization.JSONObjectWithData(pollData, options: .AllowFragments)
            
            //Looping through all polls
            if let polls = json["items"] as? [[String: AnyObject]] {
                for poll in polls {
                    
                    //Setting marker information with polldata and adding to googleMaps
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2DMake(Double(poll["latitude"] as! String)!, Double(poll["longitude"] as! String)!)
                    marker.title = poll["id"] as? String
                    marker.snippet = poll["categorie"] as? String
                    marker.icon = UIImage(named: "Poll image")
                    marker.map = googleMaps
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    /*
        Function for hiding communication menu in topbar.
    */
    func hideComLayer() {
        if comLayer.hidden == true {
            comLayer.hidden = false
        } else {
            comLayer.hidden = true
        }
    }
    
    
}