//
//  Keyboard.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 15/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}