//
//  CustomGradient.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 31/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class CustomGradient: UIView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let gradient: CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red:0/255, green:0/255, blue:0/255, alpha:0).CGColor, UIColor(red:0/255, green:0/255, blue:0/255, alpha:1).CGColor]
        
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        
        self.layer.opacity = 0.8
        self.layer.insertSublayer(gradient, atIndex: 0)
    }
}