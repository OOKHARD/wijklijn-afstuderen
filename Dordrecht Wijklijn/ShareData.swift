//
//  markerid.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 15/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation


class ShareData {
    class var sharedInstance: ShareData {
        struct Static {
            static var instance: ShareData?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ShareData()
        }
        
        return Static.instance!
    }
    
    
    var markerID : Int! //Marker ID
}