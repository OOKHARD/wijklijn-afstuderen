//
//  MapController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 28/04/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import Mapbox
import CoreLocation

class MapController: UIViewController, CLLocationManagerDelegate, MGLMapViewDelegate {
    
    var userLocation : CLLocation?
    
    let regionRadius: CLLocationDistance = 1000
    let locationManager = CLLocationManager()
    var mapBox = MGLMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestLocation()
            print("test")
        }
        
        mapBox = MGLMapView(frame: view.bounds)
        mapBox.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        view.addSubview(mapBox)
        
        getBeacons("http://ookhard.nl/school/getBeacons.php")
        
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(barButtonSystemItem: .Add, target: self,
                            action: #selector(chatClick))
        
    }
    
    func chatClick() {
        print("click")
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        userLocation = CLLocation(latitude: manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude)
        userLocation?.coordinate.latitude
        
        mapBox.setCenterCoordinate(CLLocationCoordinate2D(latitude: (userLocation?.coordinate.latitude)!, longitude: (userLocation?.coordinate.longitude)!), zoomLevel: 13, animated: true)
        
        print("locatie opgevraagd")
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    
    func getBeacons(weerUrl: String) {
        let url = NSURL(string: weerUrl)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                self.pushData(data!)
            })
        }
        
        task.resume()
    }
    
    func pushData(weatherData: NSData) {


        var names = [String]()
        
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(weatherData, options: .AllowFragments)
            
            if let beacons = json["items"] as? [[String: AnyObject]] {
                for beacon in beacons {
                    
                    if let uuid = beacon["uuid"] as? String {
                        names.append(uuid)
                    }
                    
                    let bLatitude = Double(beacon["latitude"] as! String)!
                    let bLongitude = Double(beacon["longitude"] as! String)!
                    
                    let beaconLocation = CLLocation(latitude: bLatitude, longitude: bLongitude)
                    let dcount = Double(beacon["dcount"] as! String)!
                    
                    polygonCircleForCoordinate(beaconLocation, withMeterRadius: 500 ,devices: dcount)
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
    
    func polygonCircleForCoordinate(coordinate: CLLocation, withMeterRadius: Double, devices: Double) {
        let degreesBetweenPoints = 8.0
        //45 sides
        let numberOfPoints = floor(360.0 / degreesBetweenPoints)
        let distRadians: Double = withMeterRadius / 6371000.0
        // earth radius in meters
        let centerLatRadians: Double = coordinate.coordinate.latitude * M_PI / 180
        let centerLonRadians: Double = coordinate.coordinate.longitude * M_PI / 180
        var coordinates = [CLLocationCoordinate2D]()
        //array to hold all the points
        for var index = 0; index < Int(numberOfPoints); index++ {
            let degrees: Double = Double(index) * Double(degreesBetweenPoints)
            let degreeRadians: Double = degrees * M_PI / 180
            let pointLatRadians: Double = asin(sin(centerLatRadians) * cos(distRadians) + cos(centerLatRadians) * sin(distRadians) * cos(degreeRadians))
            let pointLonRadians: Double = centerLonRadians + atan2(sin(degreeRadians) * sin(distRadians) * cos(centerLatRadians), cos(distRadians) - sin(centerLatRadians) * sin(pointLatRadians))
            let pointLat: Double = pointLatRadians * 180 / M_PI
            let pointLon: Double = pointLonRadians * 180 / M_PI
            let point: CLLocationCoordinate2D = CLLocationCoordinate2DMake(pointLat, pointLon)
            coordinates.append(point)
        }
        
        let polygon = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        mapBox.addAnnotation(polygon)
        print("Polygon toegevoegd")
    }
}