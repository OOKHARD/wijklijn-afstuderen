//
//  CustomBarButton.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 31/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class CustomBarButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    
        self.frame = CGRectMake(0, 0, 90, 90) //won't work if you don't set frame
        self.setImage(UIImage(named: "Chat image"), forState: .Normal)
        self.contentEdgeInsets = UIEdgeInsetsMake(30, 0, 0, -40)
    }
}