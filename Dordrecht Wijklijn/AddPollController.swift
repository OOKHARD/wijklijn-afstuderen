//
//  AddPollController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 15/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import GoogleMaps

class AddPollController: UIViewController, UITextViewDelegate, GMSMapViewDelegate {
    
    /*
        Addpoll variables
    */
    var googleMaps                          : GMSMapView!
    var marker                              : GMSMarker!
    var pollLat                             : CLLocationDegrees!
    var pollLong                            : CLLocationDegrees!
    
    /*
        UI Element connection with view.
    */
    @IBOutlet weak var inputQuestion        : CustomUiView!
    @IBOutlet weak var fQuestion            : CustomInputField!
    @IBOutlet weak var sQuestion            : CustomInputField!
    @IBOutlet weak var tQuestion            : CustomInputField!
    @IBOutlet weak var foQuestion           : CustomInputField!
    @IBOutlet weak var scrollView           : UIScrollView!
    @IBOutlet weak var mapView              : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set descInput placeholder
        inputQuestion.delegate = self
        inputQuestion.text =  "Type hier uw vraag…"
        
        //Set scrollview height
        scrollView.contentSize.height = 830
        
        //Init google maps
        let camera = GMSCameraPosition.cameraWithLatitude(51.811888, longitude: 4.663728, zoom: 16)
        googleMaps = GMSMapView.mapWithFrame(mapView.bounds, camera: camera)
        googleMaps.delegate = self
        googleMaps.myLocationEnabled = true
        mapView.addSubview(googleMaps)
        
        //Call hidekeyboard function, hides keyboard when tapping around in application.
        self.hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
        Button registration for sendPoll
        TODO: Connect application with backoffice Dordrecht and call and create a funcion.
    */
    @IBAction func sendPoll(sender: AnyObject) {
        var insertUrl: NSString = "http://ookhard.nl/school/insertPoll.php?t=\(pollLat)&u=\(pollLong)&v=\(inputQuestion.text!)&w=\(fQuestion.text!)&x=\(sQuestion.text!)&y=\(tQuestion.text!)&z=\(foQuestion.text!)"
        insertUrl = insertUrl.stringByReplacingOccurrencesOfString(" ", withString: "%20")
        insertUrl = insertUrl.stringByReplacingOccurrencesOfString("/n", withString: "%0A")
        addPoll(insertUrl as String)
    }
    
    /*
        Function for google maps, fire event when user tap on maps and add a marker to coordinate.
    */
    func mapView(googleMaps: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        
        //Remove markers from map
        googleMaps.clear()
        
        //setting pollLat en Pollang variables with marker coordinates
        pollLat = coordinate.latitude
        pollLong = coordinate.longitude
        
        //Add marker to map with tap coordinates
        marker = GMSMarker(position: coordinate)
        marker.map = googleMaps
    }
    
    /*
        Fires when user is editing text in textview.
        This function is used to add placeholder functionality to textView
    */
    func textViewDidBeginEditing(textView: UITextView) {
        if inputQuestion.text == "Type hier uw vraag…" {
            inputQuestion.text = nil
        }
    }
    
    /*
        Fires when user stops editing text in textview.
        This function is used to add placeholder functionality to textView
    */
    func textViewDidEndEditing(textView: UITextView) {
        if inputQuestion.text.isEmpty {
            inputQuestion.text =  "Type hier uw vraag…"
        }
    }

    /*
        Function for adding poll to database
    */
    func addPoll(pollUrl: String) {
        
        //Setting url in variable
        let url = NSURL(string: pollUrl)
        
        //Start insert poll in database task.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                
                //When poll is added without error show popup.
                let alert = UIAlertController(title: "Poll ingezonden", message: "Uw poll is succesvol verzonden naar de gemeente!", preferredStyle: UIAlertControllerStyle.Alert)
                self.presentViewController(alert, animated: true, completion: nil)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                    switch action.style{
                    case .Default:
                         //Perform backbutton segue
                        self.navigationController?.popViewControllerAnimated(true)
                        
                    case .Cancel: break
                        
                    case .Destructive: break
                    }
                }))
            })
        }
        
        task.resume()
    }
    
}