//
//  CustomBorderInput.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 30/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class CustomBorderInput: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.darkGrayColor().CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
}