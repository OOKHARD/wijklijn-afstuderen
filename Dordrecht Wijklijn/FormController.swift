//
//  FormController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 28/04/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class FormController: UIViewController, UITextFieldDelegate, UITextViewDelegate, CLLocationManagerDelegate, GMSMapViewDelegate  {
    
    /*
        Form Variables
     */
    var toPass          : UIImage?
    var passedImage     : UIImage?
    var catChoosen      : String?
    var userLocation    : CLLocation?
    var placesClient    : GMSPlacesClient?
    var googleMaps      : GMSMapView?
    var adresArray      : NSArray?
    var marker          = GMSMarker()
    
    /*
        Form let Variables
    */
    let regionRadius: CLLocationDistance = 1000
    let locationManager = CLLocationManager()
    let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
    let apikey = "AIzaSyBxGgtNBW-NmxupxUqR2caFhNf0DFw3hJM"
    
    
    /*
        UI Element connection with view.
    */
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var descInput: CustomUiView!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var adresLabel: CustomInputField!
    @IBOutlet weak var placeLabel: CustomInputField!
    @IBOutlet weak var locationWith: DisableInput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Start location authorization.
        self.locationManager.requestWhenInUseAuthorization()
        
        //Check if location use is authorizted
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //Set report image as local variable
        passedImage = toPass
        
        //Start google Places
        placesClient = GMSPlacesClient()
        
        //Set descInput placeholder
        descInput.delegate = self
        descInput.text =  "Beschrijf hier uw probleem…"
        
        //Set scrollview height
        scrollView.contentSize.height = 640
        
        //Init google maps
        let camera = GMSCameraPosition.cameraWithLatitude(51.811888, longitude: 4.663728, zoom: 16)
        googleMaps = GMSMapView.mapWithFrame(mapView.bounds, camera: camera)
        googleMaps!.myLocationEnabled = true
        googleMaps!.delegate = self
        mapView.addSubview(googleMaps!)
        
        //Call hidekeyboard function
        self.hideKeyboardWhenTappedAround() 
    }
    
    /*
        Button registration for sendButton open popup with message.
        TODO: Connect application with backoffice Dordrecht and call and create a funcion.
    */
    @IBAction func sendButton(sender: AnyObject) {
        let alert = UIAlertController(title: "Melding verzonden", message: "Uw melding is succesvol verzonden naar de gemeente!", preferredStyle: UIAlertControllerStyle.Alert)
        self.presentViewController(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                //Perform backbutton segue
                self.navigationController?.popViewControllerAnimated(true)
                
            case .Cancel: break
            
            case .Destructive: break
            }
        }))
    }
    
    /*
        Function for google maps, fire event when user tap on maps and add a marker to coordinate.
    */
    func mapView(googleMaps: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        googleMaps.clear()
        marker = GMSMarker(position: coordinate)
        marker.map = googleMaps
        locationWith.text = "Locatie zelf bepaald"
        
        //Call Function, get adress with lat long coordinates.
        getAddressForLatLng(String(coordinate.latitude), longitude: String(coordinate.longitude))
    }
    
    /*
        Fires when user is editing text in textview.
        This function is used to add placeholder functionality to textView
    */
    func textViewDidBeginEditing(textView: UITextView) {
        if descInput.text == "Beschrijf hier uw probleem…" {
            descInput.text = nil
        }
    }
    
    /*
        Fires when user stops editing text in textview.
        This function is used to add placeholder functionality to textView
    */
    func textViewDidEndEditing(textView: UITextView) {
        if descInput.text.isEmpty {
            descInput.text =  "Beschrijf hier uw probleem…"
        }
    }

    /*
        Function for updating location. This function is only called ones because we don't need continous location updates.
     */
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Set userlocation in variable
        userLocation = CLLocation(latitude: manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude)
        
        //Animate googlemaps camera to userlocation.
        googleMaps?.animateToLocation((userLocation?.coordinate)!)
        
        //Get adress from userlocation with places api
        getPlaces()
        
        //Stop updating location function.
        locationManager.stopUpdatingLocation()
    }
    
    /*
        Error function for location.
    */
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }

    /*
         Getplaces function, this function is fired when getting location with GPS.
    */
    func getPlaces() {
        
        //Call places api.
        placesClient!.currentPlaceWithCallback({ (placeLikelihoods, error) -> Void in
            guard error == nil else {
                print("Current Place error: \(error!.localizedDescription)")
                return
            }
            
            //For loop for getting places from API. This gets the most precise adress with userlocation.
            if let placeLikelihoods = placeLikelihoods {
                for likelihood in placeLikelihoods.likelihoods {
                    
                    //Set place to places api location.
                    let place = likelihood.place
                    
                    //Set adress in array. Sepperated with ","
                    self.adresArray = place.formattedAddress!.characters.split{$0 == ","}.map(String.init)
                }
            }
            
            //Set adres to UIelement adresLabel
            self.adresLabel.text = String(self.adresArray![0])
            
            //Set place to UIelement placeLabel
            self.placeLabel.text = String(self.adresArray![1])
        })
    }
    
    /*
        getAddressForLatLng function, this function is fired when getting location when user taps on marker.
    */
    func getAddressForLatLng(latitude: String, longitude: String) {
        let url = NSURL(string: "\(baseUrl)latlng=\(latitude),\(longitude)&key=\(apikey)")
        let data = NSData(contentsOfURL: url!)
        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
        
        //Looping through json response and setting variables
        if let result = json["results"] as? NSArray {
            if let address = result[0]["address_components"] as? NSArray {
                let number = address[0]["short_name"] as! String
                let street = address[1]["short_name"] as! String
                let state = address[4]["short_name"] as! String
                let zip = address[7]["short_name"] as! String
                
                self.adresLabel.text = "\(street)  \(number)"
                self.placeLabel.text = "\(zip)  \(state)"
            }
        }
    }
    
}