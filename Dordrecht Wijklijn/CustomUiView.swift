//
//  CustomUiView.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 30/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class CustomUiView: UITextView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        let shadowPath = UIBezierPath(rect: bounds)
        
        self.textContainerInset = UIEdgeInsetsMake(20, 15, 20, 15)
        self.layer.cornerRadius = 2.0
        
        self.layer.borderWidth = 0
        self.backgroundColor = UIColor.whiteColor()
        self.font = UIFont(name: "Aller", size: 14)
        self.textColor = UIColor.init(colorLiteralRed: 0.5, green: 0.52, blue: 0.54, alpha: 1.0)
        
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.08).CGColor
        self.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        self.layer.shadowOpacity = 1.0
    }
}