//
//  CustomButton.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 30/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class CustomButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.frame.size.height = 50
        self.frame.size.width = 355
        
        self.layer.backgroundColor = UIColor.init(colorLiteralRed: 5/255, green: 74/255, blue: 155/255, alpha: 1.0).CGColor
        self.layer.cornerRadius = 2.0
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.08).CGColor
        self.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        self.layer.shadowOpacity = 1.0
        
    }
}