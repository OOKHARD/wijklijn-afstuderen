//
//  AnswerPollController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 16/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit

class AnswerPollController: UIViewController {
    
    /*
        UI Element connection with view.
    */
    @IBOutlet weak var inputQuestion        : CustomUiView!
    @IBOutlet weak var fQuestion            : CustomInputField!
    @IBOutlet weak var sQuestion            : CustomInputField!
    @IBOutlet weak var tQuestion            : CustomInputField!
    @IBOutlet weak var foQuestion           : CustomInputField!
    @IBOutlet weak var scrollView           : UIScrollView!
    @IBOutlet weak var mapView              : UIView!
    @IBOutlet weak var correct1             : UIImageView!
    @IBOutlet weak var correct2             : UIImageView!
    @IBOutlet weak var correct3             : UIImageView!
    @IBOutlet weak var correct4             : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
        Answer1Btn registration, show what answer is checked.
    */
    @IBAction func answer1Btn(sender: AnyObject) {
        correct1.hidden = false
        correct2.hidden = true
        correct3.hidden = true
        correct4.hidden = true
    }

    /*
        Answer2Btn registration, show what answer is checked.
    */
    @IBAction func answer2Btn(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = false
        correct3.hidden = true
        correct4.hidden = true
    }
    
    /*
        Answer3Btn registration, show what answer is checked.
    */
    @IBAction func answer3Btn(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = true
        correct3.hidden = false
        correct4.hidden = true
    }
    
    /*
        Answer4Btn registration, show what answer is checked.
    */
    @IBAction func answer4Btn(sender: AnyObject) {
        correct1.hidden = true
        correct2.hidden = true
        correct3.hidden = true
        correct4.hidden = false
    }
    
    /*
        Button registration for sendAnswer open popup with message.
        TODO: Connect application with backoffice Dordrecht and call and create a funcion.
    */
    @IBAction func sendAnswer(sender: AnyObject) {
        let alert = UIAlertController(title: "Poll beantwoord", message: "U heeft de poll succesvol beantwoord, bedankt!", preferredStyle: UIAlertControllerStyle.Alert)
        self.presentViewController(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                //Force topnav backbutton function.
                self.navigationController?.popViewControllerAnimated(true)
                
            case .Cancel: break

            case .Destructive: break

                
            }
        }))
    }
}