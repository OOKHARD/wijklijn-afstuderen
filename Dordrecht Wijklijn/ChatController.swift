//
//  ChatController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 10/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit

class ChatController: UIViewController {

    /*
        UI Element connection with view.
    */
    @IBOutlet weak var comLayer     : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
        Button registration for contactbutton in top navigation bar.
    */
    @IBAction func chatImage(sender: AnyObject) {
        hideComLayer()
    }
    
    /*
        Button registration for phoneButton let application call number.
        TODO: Add phonennumber Dordrecht
    */
    @IBAction func phoneButton(sender: AnyObject) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://0100000000") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
                hideComLayer()
            }
        }
    }
    
    /*
        Button registration for phoneButton open mailapplication and mail to info@dordrecht.nl
        TODO: Check emailadres Dordrecht
    */
    @IBAction func mailButton(sender: AnyObject) {
        if let emailURL:NSURL = NSURL(string: "mailto:info@dordrecht.nl") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(emailURL)) {
                application.openURL(emailURL);
            }
        }
        
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Function for hiding communication menu in topbar.
    */
    func hideComLayer() {
        if comLayer.hidden == true {
            comLayer.hidden = false
        } else {
            comLayer.hidden = true
        }
    }
}

