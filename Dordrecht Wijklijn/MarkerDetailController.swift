//
//  MarkerDetailController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 15/06/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import Foundation
import UIKit

class MarkerDetailController: UIViewController {
    
    /*
        Markerdetail Variable
     */
    var toPass              : Int!
    
    /*
        Markerdetail let Variables
     */
    let shareData           = ShareData.sharedInstance

    /*
        UI Element connection with view.
    */
    @IBOutlet weak var markerImage: UIImageView!
    @IBOutlet weak var markerCat: UILabel!
    @IBOutlet weak var markerDate: UILabel!
    @IBOutlet weak var markerLocation: UILabel!
    @IBOutlet weak var markerTitle: UILabel!
    @IBOutlet weak var markerDesc: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Call function to get marker information from url
        getPoi("http://ookhard.nl/school/getMarker.php?id=\(self.shareData.markerID)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
        Button registration for backButton.
        Perform backbutton segue
    */
    @IBAction func backButton(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
        Function for getting json marker data from url
    */
    func getPoi(poiUrl: String) {
        let url = NSURL(string: poiUrl)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            dispatch_sync(dispatch_get_main_queue(), {
                self.pushMarker(data!)
            })
        }
        
        task.resume()
    }
    
    /*
        Function for looping through json marker data
    */
    func pushMarker(PoiData: NSData) {
        
        do {
            //Setting json data in let variable
            let json = try NSJSONSerialization.JSONObjectWithData(PoiData, options: .AllowFragments)
            
            //Loop through items and set UI elements
            if let pois = json["items"] as? [[String: AnyObject]] {
                for poi in pois {
                    
                    //if categorie is werkzaamheden change image to S-Werk image
                    //if categorie is event change image to S-Event image
                    if poi["categorie"] as! String == "Werkzaamheden" {
                        markerImage.image = UIImage(named: "S-Werk image")
                        markerImage.frame = CGRectMake(23, 16, 24, 14)
                        markerDate.text = "Van \(poi["van_date"]!) tot \(poi["tot_date"]!)"
                    } else {
                        markerImage.image = UIImage(named: "S-Event image")
                        markerImage.frame = CGRectMake(23, 14, 22, 22)
                        markerDate.text = "Van \(poi["van_time"]!) tot \(poi["tot_time"]!)"
                    }
                    
                    markerCat.text = poi["categorie"] as? String
                    markerLocation.text = "Locatie: \(poi["locatie"] as! String)"
                    markerTitle.text = poi["title"] as? String
                    markerDesc.text = poi["desc_lang"] as! String
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
    }
}