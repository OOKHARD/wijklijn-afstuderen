//
//  StatusDetailController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 31/05/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import GoogleMaps

class StatusDetailController: UIViewController, GMSMapViewDelegate {
    
    /*
        Statusdetail Variables
    */
    var googleMaps                  : GMSMapView?
    
    /*
        UI Element connection with view.
     */
    @IBOutlet weak var mapView      : UIView!
    @IBOutlet weak var scrollView   : UIScrollView!
    @IBOutlet weak var comLayer     : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize.height = 780
        
        //Hide communication menu.
        comLayer.hidden = true
        
        let camera = GMSCameraPosition.cameraWithLatitude(51.811264, longitude: 4.668095, zoom: 16)
        
        googleMaps = GMSMapView.mapWithFrame(mapView.bounds, camera: camera)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(51.811264, 4.668095)
        marker.map = googleMaps
        
        mapView.addSubview(self.googleMaps!)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
        prepareForSegue fires when segue is performed.
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "chatSegue" {
            
            //Hide navigationbar when segueID is chatSegue
            let destinationController = segue.destinationViewController as! ChatController
            destinationController.hidesBottomBarWhenPushed = true
            
            //Hide communication menu.
            hideComLayer()
        }
    }
    
    /*
        Button registration for contactbutton in top navigation bar.
    */
    @IBAction func contactButton(sender: AnyObject) {
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Button registration for phoneButton let application call number.
        TODO: Add phonennumber Dordrecht
    */
    @IBAction func phoneButton(sender: AnyObject) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://0100000000") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
                hideComLayer()
            }
        }
    }
    
    /*
        Button registration for phoneButton open mailapplication and mail to info@dordrecht.nl
        TODO: Check emailadres Dordrecht
    */
    @IBAction func mailButton(sender: AnyObject) {
        if let emailURL:NSURL = NSURL(string: "mailto:info@dordrecht.nl") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(emailURL)) {
                application.openURL(emailURL);
            }
        }
        
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Function for hiding communication menu in topbar.
    */
    func hideComLayer() {
        if comLayer.hidden == true {
            comLayer.hidden = false
        } else {
            comLayer.hidden = true
        }
    }
}
