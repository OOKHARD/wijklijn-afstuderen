//
//  CameraController.swift
//  Dordrecht Wijklijn
//
//  Created by Tom Harms on 26/04/16.
//  Copyright © 2016 Tom Harms. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: UIViewController, UIImagePickerControllerDelegate {

    
    /*
        Camera Variables
    */
    var captureSession              : AVCaptureSession?
    var stillImageOutput            : AVCaptureStillImageOutput?
    var previewLayer                : AVCaptureVideoPreviewLayer?
    var image                       : UIImage?
    var captureDevice               : AVCaptureDevice?
    
    /*
        UI Element connection with view.
    */
    @IBOutlet weak var cameraView   : UIView!
    @IBOutlet weak var cameraButton : UIButton!
    @IBOutlet weak var comLayer     : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Hide communication menu.
        comLayer.hidden = true
        
        //Init camera
        captureSession = AVCaptureSession()
        captureSession?.sessionPreset = AVCaptureSessionPreset1920x1080
        let backCamera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        /*
            Get camera input and set camera settings.
            Add camera input to cameraView.
         */
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            captureSession?.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
            
            if ((captureSession?.canAddOutput(stillImageOutput)) != nil) {
                captureSession?.addOutput(stillImageOutput)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect
                previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                
                cameraView.layer.addSublayer(previewLayer!)
                
                captureSession?.startRunning()
            }
            
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //Setting frame of cameraoutput.
        previewLayer?.frame = cameraView.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
       
        //Pass image to next viewcontroller when segueID is inputSegue
        if (segue.identifier == "inputSegue") {
            let svc = segue.destinationViewController as! FormController;
            
            svc.toPass = self.image
            
        }
        
        //Hide navigationbar when segueID is chatSegue
        if segue.identifier == "chatSegue" {
            
            //Hide navigationbar in chatView.
            let destinationController = segue.destinationViewController as! ChatController
            destinationController.hidesBottomBarWhenPushed = true
            
            //Hide communication menu.
            hideComLayer()
        }
    }
    
    /*
        Button registration for making picture.
     */
    @IBAction func makePicture(sender: UIButton) {
        didPressTakePhoto()
    }
    
    /*
        Button registration for contactbutton in top navigation bar.
     */
    @IBAction func contactButton(sender: AnyObject) {
        //Hide communication menu.
        hideComLayer()
    }
    
    /*
        Button registration for phoneButton let application call number.
        TODO: Add phonennumber Dordrecht
     */
    @IBAction func phoneButton(sender: AnyObject) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://0100000000") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
                hideComLayer()
            }
        }
    }
    
    /*
        Button registration for phoneButton open mailapplication and mail to info@dordrecht.nl
        TODO: Check emailadres Dordrecht
     */
    @IBAction func mailButton(sender: AnyObject) {
        if let emailURL:NSURL = NSURL(string: "mailto:info@dordrecht.nl") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(emailURL)) {
                application.openURL(emailURL);
            }
        }
        
        //Hide communication menu.
        hideComLayer()
    }

    /*
        Function for temporary saving picture in variable.
     */
    func didPressTakePhoto() {
        
        // Check if imageOuput is filled with camera stream.
        if let cameraOutput = stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo) {
            
            //setting imageOutput orientation.
            cameraOutput.videoOrientation = AVCaptureVideoOrientation.Portrait
            
            //Creating stillimage of camraoutput.
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(cameraOutput, completionHandler: {
                (sampleBuffer, error) in
                
                /*
                    Saving image and export image to jpeg.
                 */
                if sampleBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    self.image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                    
                    self.performSegueWithIdentifier("inputSegue", sender: self)
                }
            })
        }
        
    }
    
    /*
        Function for hiding communication menu in topbar.
    */
    func hideComLayer() {
        if comLayer.hidden == true {
            comLayer.hidden = false
        } else {
            comLayer.hidden = true
        }
    }
}
